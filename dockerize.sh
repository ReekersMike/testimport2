#!/bin/bash

if [ "$1" == "build" ]; then
    docker-compose build
    cd frontend
    npm install
    npm run build
elif [ "$1" == "run" ]; then
    docker-compose up -d
elif [ "$1" == "stop" ]; then
    docker-compose down
fi



# 
# TODO: Implement the logic for the commands below. 
# yeh so doesnt make sense we only run the backend so we also just starting the front end:)
# 
# Usage:
#  build - Create docker container for the backend application
#  run   - Run the backend as a container (you can use port 5000 in your port mapping)
#  stop  - Stop the backend container
