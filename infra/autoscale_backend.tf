#Launch template for hig availability backend

resource "aws_launch_template" "chess_backend_template" {
  name   = "ChessBackendTemplate"
  image_id      = "ami-09cd747c78a9add63"
  instance_type = "t2.micro"
 
  network_interfaces {
    associate_public_ip_address = true
    subnet_id = values(aws_subnet.main)[0].id
    security_groups = [aws_security_group.chess_backend_sg.id]
  }

  tag_specifications {
    resource_type = "instance"

    tags = {
      Name = "Chess_Backend"
    }
  }

  user_data = filebase64("${path.module}/backend_userdata.sh")
  key_name  = "KeyPair1"
}

resource "aws_autoscaling_group" "ChessScalingGroup" {
  vpc_zone_identifier = values(aws_subnet.main)[*].id

  desired_capacity   = 2
  max_size           = 4
  min_size           = 2

  launch_template {
    id      = aws_launch_template.chess_backend_template.id
    version = "$Latest"
  }
}

resource "aws_security_group" "chess_backend_sg" {
  name_prefix = "ChessBackendSG"
   vpc_id      = aws_vpc.main.id
  ingress {
    from_port   = 5000
    to_port     = 5000
    protocol    = "tcp"
    cidr_blocks = ["10.0.3.0/24"]
  }
    ingress {
    from_port   = 5000
    to_port     = 5000
    protocol    = "tcp"
    cidr_blocks = ["10.0.4.0/24"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["10.0.3.0/24"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["10.0.4.0/24"]
  }
  egress{
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#load balancer

resource "aws_lb" "backend_lb" {
  name               = "BackendLoadBalancer"
  internal           = false
  load_balancer_type = "application"

  subnets = values(aws_subnet.main)[*].id

  security_groups    = [aws_security_group.backend_lb_sg.id]
}

resource "aws_lb_target_group" "backend_lb_tg" {
  name        = "BackendBalancerTG"
  port               = 5000
  protocol           = "HTTP"
  vpc_id             = aws_vpc.main.id
  target_type        = "instance"
}

resource "aws_lb_listener" "backend_lb_lis" {
  load_balancer_arn = aws_lb.backend_lb.arn
  port              = "5000"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_lb_target_group.backend_lb_tg.arn
    type             = "forward"
  }
}

resource "aws_autoscaling_attachment" "backend_asg_attachment" {
  autoscaling_group_name = aws_autoscaling_group.ChessScalingGroup.name
  lb_target_group_arn   = aws_lb_target_group.backend_lb_tg.arn
}

resource "aws_security_group" "backend_lb_sg" {
  name_prefix = "backend_lb_sg"
   vpc_id      = aws_vpc.main.id
  ingress {
    from_port   = 5000
    to_port     = 5000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress{
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}