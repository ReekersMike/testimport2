terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region = "us-east-1"
}

resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "vpc_chess_1"
  }
}

resource "aws_subnet" "main" {
  vpc_id     = aws_vpc.main.id
  for_each = var.subnet_details
  cidr_block = each.value
  availability_zone = each.key
  tags = {
    #Adding a name variable to this would be nice so they are not both called Main
    Name = "Main"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "igw_chess_01"
  }
}

resource "aws_route_table" "main" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "rt_chess_01"
  }
}

resource "aws_route_table_association" "subnet_association" {
  for_each = aws_subnet.main
  subnet_id = each.value.id
  route_table_id = aws_route_table.main.id
}
