#Single CI EC2 Instance.

resource "aws_instance" "ci_backend" {
  ami           = "ami-09cd747c78a9add63"
  instance_type = "t2.micro"
  vpc_security_group_ids = [aws_security_group.ci_backend.id]
  key_name      = "KeyPair1"
  user_data = filebase64("${path.module}/ci_backend_userdata.sh")

  tags = {
    Name = "CI_Chess_backend"
  }
}

resource "aws_security_group" "ci_backend" {
  name_prefix = "ci_backend"
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 5000
    to_port     = 5000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress{
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}